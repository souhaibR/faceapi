from django.urls import path

from . import api

urlpatterns = [
    path('faces', api.faces, name='faces'),
    path('emotion', api.emotion, name='emotion'),
    path('attributes',api.attributes, name='attributes'),
    path('mask',api.detect_mask,name='mask'),
    path('verify',api.verifyFaces,name='verify'),
    path('createModel',api.CreateModel,name='createModel'),
    path('authenticate',api.authenticateUser,name='authenticate'),
    path('logout',api.logoutUser,name='logout')
]

 