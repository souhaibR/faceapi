from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import cv2
import requests
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from face_API.face.detection.model import RetinaFace
from face_API.face.emotions.model import Xception
from face_API.face.mask.model import Mask_detection
from face_API.face.verification.model import FaceVerif
from face_API.face.attributes.model import FaceAttributes
from face_API.models import CustomUser, CustomUserManager
from face_API.auth_backend import PasswordlessAuthBackend
from django.contrib.auth import  login,logout
import numpy as np
from django.core.exceptions import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
import pickle
#Load Models
try:
    Retina=RetinaFace()
    emotion_classifier=Xception(Retina)
    mask_classifier=Mask_detection(Retina)
    attributes_classifier=FaceAttributes(Retina)
    verif= FaceVerif(Retina)   
except IOError as f:
    print('Models were not properly loaded')

def validate(uploaded_file):
    content_type = uploaded_file.content_type.split('/')[0]
    if content_type in settings.UPLOAD_EXTENSIONS:
        if uploaded_file.size > settings.MAX_CONTENT_LENGTH:
            raise  ValidationError(('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.MAX_CONTENT_LENGTH), filesizeformat(uploaded_file.size)))
    else:
        raise  ValidationError('File type is not supported')  
@csrf_exempt
def faces(request):
    
    if len(request.FILES)==0:
        return HttpResponse(" Please Choose a file",status=400)
    try:
        validate(request.FILES['file'])
    except ValidationError as v: 
        return HttpResponse(v,400)
    img_raw = cv2.imdecode(np.fromstring(request.FILES['file'].read(),np.uint8), cv2.IMREAD_COLOR)
    json=Retina.json(img_raw)
    return JsonResponse(json, safe=False) 
@csrf_exempt
def emotion(request):
    if len(request.FILES)==0:
        return HttpResponse(" Please Choose a file",status=404)
    try:
        validate(request.FILES['file'])
    except ValidationError as v: 
        return HttpResponse(v,400)
    img_raw = cv2.imdecode(np.fromstring(request.FILES['file'].read(),
                                    np.uint8), cv2.IMREAD_UNCHANGED)
    json=emotion_classifier.json(img_raw)
    return JsonResponse(json, safe=False)
@csrf_exempt
def detect_mask(request):
    if len(request.FILES)==0:
        return HttpResponse(" Please Choose a file",status=404)
    try:
        validate(request.FILES['file'])
    except ValidationError as v: 
        return HttpResponse(v,400)
    img_raw = cv2.imdecode(np.fromstring(request.FILES['file'].read(),
                                    np.uint8), cv2.IMREAD_UNCHANGED)
    json=mask_classifier.json(img_raw)
    return JsonResponse(json, safe=False)
@csrf_exempt
def attributes(request):
    if len(request.FILES)==0:
        return HttpResponse(" Please Choose a file",status=404)
    try:
        validate(request.FILES['file'])
    except ValidationError as v: 
        return HttpResponse(v,400)
    img_raw = cv2.imdecode(np.fromstring(request.FILES['file'].read(),
                                    np.uint8), cv2.IMREAD_UNCHANGED)
    json=attributes_classifier.json(img_raw)
    return JsonResponse(json, safe=False)
@csrf_exempt
def verifyFaces(request):
    if len(request.FILES)==0:
        return HttpResponse(" Please Choose a file",status=404)
    try:
        validate(request.FILES['file'])
        validate(request.FILES['file2'])
    except ValidationError as v: 
        return HttpResponse(v,400)
    img_raw1 = cv2.imdecode(np.fromstring(request.FILES['file'].read(),
                                        np.uint8), cv2.IMREAD_COLOR)
    img_raw2 = cv2.imdecode(np.fromstring(request.FILES['file2'].read(),
                                        np.uint8), cv2.IMREAD_COLOR)

    json=verif.json(img_raw1,img_raw2)
    
    return JsonResponse(json, safe=False)
@csrf_exempt
def CreateModel(request):
    images=[]
    if len(request.FILES)!=6:
        return HttpResponse('please upload 6 pictures',status=400)
    for f in request.FILES.values():
        try:
            validate(f)
        except ValidationError as v: 
            return HttpResponse(v,400)
        else :
            images.append(cv2.imdecode(np.fromstring(f.read(),np.uint8), cv2.IMREAD_COLOR))
    username=request.POST['username']
    email=request.POST['email']
    try:
        user = CustomUser.objects.get(username=username)
        return HttpResponse('User already exists',status=302)
    except CustomUser.DoesNotExist:
        try:
            model=verif.createUserModel(images)
        except FileNotFoundError as e:
            return HttpResponse('could not create user model',500)
        else:
            CustomUser.objects.create_user(email,username)
            file=open("face_API/media/uploads/"+username+".pkl", 'wb')
            pickle.dump(model, file)
            file.close()
            return HttpResponse(200)  
@csrf_exempt
def authenticateUser(request):
    auth=PasswordlessAuthBackend()
    img_raw = cv2.imdecode(np.fromstring(request.FILES['file'].read(),np.uint8), cv2.IMREAD_COLOR)
    uname=request.POST['username']
    user = auth.authenticate(username=uname)
    if user:
        try:
            file=open("face_API/media/uploads/"+user.username+".pkl","rb")
            usermodel=pickle.load(file)
            #file.close()
            result=verif.authenticate(img_raw,usermodel)
            if result:
                login(request,user,'face_API.auth_backend.PasswordlessAuthBackend')
                return HttpResponse('logged in ',status=200)
            else:
                return HttpResponse('Wrong user',status=401)
        except ValidationError as e:
            return HttpResponse (e.message,status=401)
    else: 
        return HttpResponse('username not found',status=404)
@csrf_exempt
def logoutUser(request):
    logout(request)
    return HttpResponse('logged out ',status=200)